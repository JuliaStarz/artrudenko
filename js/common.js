$(function() {
    $(".totop").click(function(e){
        e.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
    });

    /* form validation */
	$('#phone').mask('+7(000)000-00-00', {placeholder: '+7(___)___-__-__'});
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                event.preventDefault();

                if (form.checkValidity() === true) {
                    $.ajax({
                        type: form.getAttribute('method'),
                        url: form.getAttribute('action'),
                        data: serialize.simple(form.classList[0]),
            
                        complete: function(data, status) {
                            form.reset();
                            form.classList.add('d-none');
                            document.getElementById('modal-message').classList.remove('d-none');
                        }
                    });
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    window.serialize = {
        simple: function (form) {
            var formClass = '.' + form;
            var formel = document.querySelectorAll(formClass),
                inputs = formel[0].querySelectorAll("input, select, textarea"),
                obj = {}, key;
            for (key in inputs) {
                if (inputs[key].tagName) {
                    if (inputs[key].type === "checkbox") {
                        obj[inputs[key].name] = inputs[key].checked === true ? inputs[key].value : false;
                    } else {
                        obj[inputs[key].name] = inputs[key].value;
                    }
                }
            }
            //console.log(obj);
            return obj;
        }
    }

    $('#requestForm').on('show.bs.modal', function (e) {
        document.getElementById('request').classList.remove('d-none', 'was-validated');
        document.getElementById('modal-message').classList.add('d-none');
    })

    /* index page slider */
    if(document.getElementById('index-slider')) {
        $('#index-slider').slick({
            dots: true,
            arrows: false,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true,
            cssEase: 'linear',
            autoplay: true
        });
    }
    /* portfolio page slider */
    if(document.getElementById('portfolio_item-slider')) {
        $('#portfolio_item-slider').slick({
            arrows: false,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true,
            cssEase: 'linear',
            //autoplay: true,
            asNavFor: '.page-slide-gallery-nav'
        });
        $('.page-slide-gallery-nav').slick({
            arrows: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.page-slide-gallery',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 567,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });
    }
})